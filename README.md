K8 Excercise chart Folder
Create Helm Chart for Java App

created helm chart boilerplate for your application with chart-name java-app
 helm create java-app


This will generate java-app folder with chart files

cleaned up all unneeded contents from java-app folder

created template files for db-config.yaml, db-secret.yaml, java-app-deployment.yaml, java-app-ingress.yaml, java-app-service.yaml

created values-override.yaml and set all the correct values there
set default chart values in values.yaml file

..The ingress.hostName must be set to my-java-app.com for Minikube 

validated that the chart is correct and debug any issues, do a dry-run

helm install -f values-override.yaml(original -value-folder) mysql-java-app(release-name) java-app/(chart folder) --dry-run --debug
helm install -f values-override.yaml mysql-java-app java-app/ --dry-run --debug

 dry-run shows the k8s manifest files with correct values, everything is working
 
created the chart release

helm install -f values-override.yaml mysql-java-app java-app/


extracted the chart java-app folder and host into its own new git repository java-app-chart

